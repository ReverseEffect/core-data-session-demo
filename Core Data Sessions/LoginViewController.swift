//
//  LoginViewController.swift
//  Core Data Sessions
//
//  Created by Robert on 1/4/17.
//  Copyright © 2017 ReverseEffect Applications. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginRegisterButtonPressed(sender: UIButton) {
        if usernameTextField.text == "" || passwordTextField.text == "" {
            showAlert(title: "Error", message: "Please fill in all fields.")
        }
        else {
            if sender.tag == 101 {
                // Handle sign-in
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                request.returnsObjectsAsFaults = false
                
                do {
                    let results = try getManagedObjectContext().fetch(request)
                    
                    if results.count > 0 {
                        for result in results as! [NSManagedObject] {
                            if let username = result.value(forKey: "username") as? String {
                                if let password = result.value(forKey: "password") as? String {
                                    if username == usernameTextField.text && password == passwordTextField.text {
                                        let newSession = NSEntityDescription.insertNewObject(forEntityName: "Session", into: getManagedObjectContext())
                                        newSession.setValue(usernameTextField.text, forKey: "username")
                                        
                                        do {
                                            try getManagedObjectContext().save()
                                            print("Context has been saved.")
                                        }
                                        catch {
                                            print("Unable to save context.")
                                        }
                                        
                                        dismiss(animated: true, completion: nil)
                                        
                                        return
                                    }
                                }
                            }
                        }
                        
                        showAlert(title: "Unable to Sign In", message: "Username and/or password is incorrect.")
                    }
                    else if results.count == 0 {
                        showAlert(title: "Unable to Sign In", message: "Username and/or password is incorrect.")
                    }
                }
                catch {
                    print("Unable to fetch results of request.")
                }
            }
            else if sender.tag == 102 {
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                request.returnsObjectsAsFaults = false
                
                do {
                    let results = try getManagedObjectContext().fetch(request)
                    
                    if results.count > 0 {
                        for result in results as! [NSManagedObject] {
                            if let username = result.value(forKey: "username") as? String {
                                if username == usernameTextField.text {
                                    showAlert(title: "Error", message: "Please choose a different username.")
                                    return
                                }
                            }
                        }
                        
                        let registeringUser = NSEntityDescription.insertNewObject(forEntityName: "User", into: getManagedObjectContext())
                        registeringUser.setValue(usernameTextField.text, forKey: "username")
                        registeringUser.setValue(passwordTextField.text, forKey: "password")
                        
                        let newSession = NSEntityDescription.insertNewObject(forEntityName: "Session", into: getManagedObjectContext())
                        newSession.setValue(usernameTextField.text, forKey: "username")
                    }
                    else if results.count == 0 {
                        let registeringUser = NSEntityDescription.insertNewObject(forEntityName: "User", into: getManagedObjectContext())
                        registeringUser.setValue(usernameTextField.text, forKey: "username")
                        registeringUser.setValue(passwordTextField.text, forKey: "password")
                        
                        let newSession = NSEntityDescription.insertNewObject(forEntityName: "Session", into: getManagedObjectContext())
                        newSession.setValue(usernameTextField.text, forKey: "username")
                    }
                    
                    do {
                        try getManagedObjectContext().save()
                        print("Context has been saved.")
                    }
                    catch {
                        print("Unable to save context.")
                    }
                    
                    dismiss(animated: true, completion: nil)
                }
                catch {
                    print("Unable to fetch results of request.")
                }
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil);
    }
    
    func getManagedObjectContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
