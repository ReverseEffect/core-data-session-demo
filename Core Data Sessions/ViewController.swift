//
//  ViewController.swift
//  Core Data Sessions
//
//  Created by Robert on 1/4/17.
//  Copyright © 2017 ReverseEffect. All rights reserved.
//

/*
 * About this project.
 * This project was an idea based off of the lesson's project, but has taken other turns.
 */

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Session")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try getManagedObjectContext().fetch(request)
            
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if let username = result.value(forKey: "username") as? String {
                        usernameLabel.text = username
                    }
                }
            }
            else if results.count == 0 {
                performSegue(withIdentifier: "loginSegue", sender: nil)
            }
        }
        catch {
            print("Unable to fetch results of request.")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logoutSegue" {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Session")
            request.returnsObjectsAsFaults = false
            
            do {
                let results = try getManagedObjectContext().fetch(request)
                
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        getManagedObjectContext().delete(result)
                    }
                    
                    do {
                        try getManagedObjectContext().save()
                        print("Context has been saved.")
                    }
                    catch {
                        print("Unable to save context.")
                    }
                }
            }
            catch {
                print("Unable to fetch results of request.")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getManagedObjectContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}

